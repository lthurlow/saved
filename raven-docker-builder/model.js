
let name = "foundry_container_"+Math.random().toString().substr(-6);

topo = {
  name: name,
  nodes: [ deb('builder') ],
}

function deb(name) {
  return {
    name: name,
    image: 'debian-buster',
    memory: { capacity: GB(2) },
    proc: { cores: 2 },
    mounts: [
      {source: env.PWD+'/../..', point: '/tmp/foundry'}
    ],
  }
}
